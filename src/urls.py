
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

from filebrowser.sites import site
from ckeditor_uploader import urls as ck_urls


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^ckeditor/', include(ck_urls)),


] + (static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT))
urlpatterns += i18n_patterns('',
    url(r'', include('apps.web.urls', namespace='web')),
    )
handler404 = 'apps.web.views.page_404'
handler500 = 'apps.web.views.page_500'
