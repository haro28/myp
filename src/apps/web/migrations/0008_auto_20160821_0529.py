# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-08-21 05:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0007_infosite_video'),
    ]

    operations = [
        migrations.AddField(
            model_name='infosite',
            name='activar_clientes',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='infosite',
            name='activar_idiomas',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='infosite',
            name='activar_trabajo',
            field=models.BooleanField(default=True),
        ),
    ]
