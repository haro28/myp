# -*- coding: utf-8 -*-
from urlparse import urlparse

from django.db import models
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse

from geoposition.fields import GeopositionField
from filebrowser.fields import FileBrowseField
from ckeditor.fields import RichTextField


from apps.core.models import AuditableModel, SlugModel, PositionModel


class InfoSite(AuditableModel):
    site = models.CharField("URL del sitio", max_length=60, blank=True,
        help_text='Ingrese la url actual del sitio sin el slash final')

    ga = models.CharField('Google Analytics', max_length=24, blank=True,
       help_text='''Opcional: Inserte el código que Google Analytics le
       proporciona con el formato: UA-XXXXX-X''')
    video = FileBrowseField('Video secciones', max_length=700,
        directory='infosite', extensions=['.mp4', '.avi', '.mpg'],
        help_text=u'Videos en formato mp4, avi, mpg')

    activar_idiomas = models.BooleanField(default=True)
    activar_trabajo = models.BooleanField(default=True)
    activar_clientes = models.BooleanField(default=True)

    telefono = models.CharField('Teléfono', max_length=120, blank=True)
    email = models.CharField('Email', max_length=120, blank=True)
    direccion = models.CharField('Dirección', max_length=120, blank=True)
    logo_empresa = FileBrowseField('Logo de la empresa', max_length=700,
        directory='infosite', extensions=['.jpg', '.png', '.gif'],
        help_text=u'Tamaño recomendado:1110x137')
    # formulario contacto
    email_contacto = models.EmailField('Email Contacto', blank=True, null=True, help_text='A este email llegarán los formularios de contacto')
    # REDES SOCIALES
    facebook = models.URLField('Facebook', blank=True)
    linkedin = models.URLField('Linkedin', blank=True)
    gmail = models.URLField('Gmail', blank=True)
    youtube = models.URLField('youtube', blank=True)
    twitter = models.URLField('Twitter', blank=True)
    coordenadas = GeopositionField('Coordenadas')

    def __unicode__(self):
        return u'Información del Sitio'

    class Meta:
        verbose_name_plural = u'Información del Sitio'

    def save(self, *args, **kwargs):
        site = Site.objects.get(id=settings.SITE_ID)
        site.domain = urlparse(self.site).netloc
        site.name = settings.PROJECT_NAME
        site.save()
        super(InfoSite, self).save(*args, **kwargs)


class Home(models.Model):
    titulo_bienvenida = models.CharField('Título de bienvenida', max_length=100)
    texto_bienvenida = RichTextField('Texto de Bienvenida')

    def __unicode__(self):
        return u'Home'

    class Meta:
        verbose_name = 'Home'
        verbose_name_plural = 'Home'


class SeccionServicios(models.Model):
    titulo_seccion_servicios = models.CharField('Titulo servicios', max_length=100)
    texto_servicios = RichTextField('Texto servicios')

    def __unicode__(self):
        return u'Sección servicios'

    class Meta:
        verbose_name = 'Sección servicios'
        verbose_name_plural = 'Sección servicios'


class Servicios(SlugModel, PositionModel):
    descripcion_servicio = RichTextField('Descripción servicios', blank=True, null=True)
    imagen_principal = FileBrowseField('Imagen principal del servicio', max_length=700,
        directory='servicios', extensions=['.jpg', '.png', '.gif'],
        help_text=u'Tamaño recomendado:345x250')

    def get_absolute_url(self):
        return reverse('web:servicios_detalle', kwargs={'slug': self.slug})

    def __unicode__(self):
        return unicode(self.nombre)

    class Meta:
        verbose_name = 'Servicio'
        verbose_name_plural = 'Servicios'
        ordering = ['position']


class ImagenesServicio(PositionModel):
    fk_servicio = models.ForeignKey(Servicios)
    titulo_imagen = models.CharField('Título Imagen', max_length=100)
    imagen = FileBrowseField('Imagen', max_length=700,
        directory='imagenes_servicios', extensions=['.jpg', '.png', '.gif'],
        help_text=u'Tamaño recomendado:345x266')

    def __unicode__(self):
        return self.titulo_imagen

    class Meta:
        verbose_name = 'Imagen Servicio'
        verbose_name_plural = 'Imagenes Servicios'
        ordering = ['position']


class NuestraHistoria(models.Model):
    titulo = models.CharField('Título Historia', max_length=100)
    texto = RichTextField('Texto')

    def __unicode__(self):
        return u'Nuestra Historia'

    class Meta:
        verbose_name = 'Nuestra Historia'
        verbose_name_plural = 'Nuestra Historia'


class QuienesSomos(models.Model):
    titulo_quienes_somos = models.CharField('Título quiénes somos', max_length=80)
    texto_quienes_somos = RichTextField('Texto quienes somos', blank=True, null=True)
    titulo_nuestros_socios = models.CharField('Título nuestros socios', max_length=80)
    texto_nuestros_socios = RichTextField('Texto nuestros socios', blank=True, null=True)

    def __unicode__(self):
        return u'Quienes Somos'

    class Meta:
        verbose_name = 'Quienes Somos'
        verbose_name_plural = 'Quienes Somos'


class OtrosPuestos(PositionModel):
    CHOICES_BLOQUES = (
        ('IZQUIERDO', 'IZQUIERDO'),
        ('DERECHO', 'DERECHO')
    )
    fk_quienes_somos = models.ForeignKey(QuienesSomos)
    titulo_puesto = models.CharField('Puesto', max_length=100)
    bloque = models.CharField('Bloque',  choices=CHOICES_BLOQUES, max_length=100)

    def __unicode__(self):
        return self.titulo_puesto

    class Meta:
        verbose_name = 'Puesto'
        verbose_name_plural = 'Puestos'
        ordering = ['position']


class SeccionPoliticas(models.Model):
    titulo_mision = models.CharField('Título misión', max_length=50)
    texto_mision = RichTextField('Texto misión')
    titulo_vision = models.CharField('Título vision', max_length=50)
    texto_vision = RichTextField('Texto vision')
    titulo_politicas_ambientales = models.CharField('Titulo políticas ambientales', max_length=100)
    texto_politicas_ambientales = RichTextField('Texto políticas embientales', blank=True, null=True)
    pdf = FileBrowseField('PDF', max_length=700,
            directory='pdf', extensions=['.doc', '.docx', '.pdf'],
            help_text=u'Suba solamente archivos en formato  doc, docx y pdf')

    def __unicode__(self):
        return u'Mision / Vision / Políticas'

    class Meta:
        verbose_name = 'Mision / Vision / Políticas'
        verbose_name_plural = 'Mision / Vision / Políticas'


class Valores(PositionModel):
    titulo_valor = models.CharField('Título valor', max_length=80)
    texto_valor = RichTextField('Texto valor')
    imagen_valor = FileBrowseField('Imagen valor', max_length=700,
        directory='valores', extensions=['.jpg', '.png'],
        help_text=u'Tamaño recomendado:46x43')

    def __unicode__(self):
        return self.titulo_valor

    class Meta:
        verbose_name = 'Valor'
        verbose_name_plural = 'Valores'


class Socios(PositionModel):
    nombre = models.CharField('Nombre', max_length=100)
    puesto = models.CharField('Puesto', max_length=80)
    imagen = FileBrowseField('Imagen categoría del trabajo', max_length=700,
        directory='socios', extensions=['.jpg', '.png', '.gif'],
        help_text=u'Tamaño recomendado:406x320')
    cv = FileBrowseField('CV', max_length=700,
        directory='cv', extensions=['.doc', '.docx', '.pdf'],
        help_text=u'Suba solamente archivos en formato  doc, docx y pdf')

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Socio'
        verbose_name_plural = 'Socios'
        ordering = ['position']


class Logros(PositionModel):
    fk_socio = models.ForeignKey(Socios)
    logro = models.CharField('Logro', max_length=80)

    def __unicode__(self):
        return self.logro

    class Meta:
        verbose_name = 'Logro'
        verbose_name_plural = 'Logros'
        ordering = ['position']


class Equipamiento(PositionModel):
    CHOICES_BLOQUES = (
            ('izquierdo', 'izquierdo'),
            ('central', 'central'),
            ('derecho', 'derecho')
        )

    bloque = models.CharField('Elegir bloque', max_length=100, choices=CHOICES_BLOQUES)
    equipamiento = models.CharField('Equipamiento', max_length=200)

    def contain_detalle(self):
        return self.detalleequipamiento_set.all()

    def __unicode__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Equipamiento'
        verbose_name_plural = 'Equipamientos'
        ordering = ['position']


class DetalleEquipamiento(PositionModel):
    fk_equipamiento = models.ForeignKey(Equipamiento, blank=True, null=True)
    detalle_equipamiento = models.CharField('Detalle equipamiento', max_length=100)

    def __unicode__(self):
        return self.detalle_equipamiento

    class Meta:
        verbose_name = 'Detalle Equipamiento'
        ordering = 'Position'
        ordering = ['position']


class SeccionTrabajo(models.Model):
    titulo_trabajo = models.CharField('Título trabajo', max_length=100)
    texto = RichTextField('Texto Trabajo')

    def __unicode__(self):
        return u'Sección Trabajo'

    class Meta:
        verbose_name = 'Sección Trabajo'


class Trabajo(PositionModel):
    titulo_trabajo = models.CharField('Título trabajo', max_length=100)
    text_trabajo = RichTextField('Texto Trabajo', blank=True, null=True)

    def __unicode__(self):
        return self.titulo_trabajo

    class Meta:
        verbose_name = u'Trabajo'
        verbose_name_plural = u'Trabajos'
        ordering = ['position']


class ImagenTrabajos(PositionModel):
    fk_trabajo = models.ForeignKey(Trabajo)
    titulo_trabajo = models.CharField('Título de la imagen', max_length=100)
    imagen_trabajo = FileBrowseField('Imagen del trabajo', max_length=700,
        directory='trabajos', extensions=['.jpg', '.png', '.gif'],
        help_text=u'Tamaño recomendado:445x320')

    def __unicode__(self):
        return self.titulo_trabajo

    class Meta:
        verbose_name = u'Trabajo'
        verbose_name_plural = u'Trabajos'
        ordering = ['position']


class SeccionClientes(models.Model):
    titulo_seccion_clientes = models.CharField('Título sección clientes', max_length=100)
    texto = RichTextField('Texto', blank=True, null=True)

    def __unicode__(self):
        return u'Sección clientes'

    class Meta:
        verbose_name = 'Sección clientes'
        verbose_name_plural = 'Sección clientes'


class Clientes(PositionModel):
    fk_seccion_clientes = models.ForeignKey(SeccionClientes)
    nombre = models.CharField('Nombre', max_length=100)
    imagen_logo = FileBrowseField('Logo del cliente', max_length=700,
        directory='clientes', extensions=['.jpg', '.png', '.gif'],
        help_text=u'Tamaño recomendado:200x150')

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['position']


class Contacto(models.Model):
    fecha = models.DateTimeField('Fecha', auto_now_add=True, blank=True, null=True)
    nombres = models.CharField('Nombres', max_length=200)
    email = models.EmailField('Email')
    telefono = models.CharField('Teléfono', max_length=100, blank=True, null=True)
    compania = models.CharField('Compañia', max_length=100, blank=True, null=True)
    asunto = models.CharField('Asunto', max_length=100)
    mensaje = models.TextField('Mensaje')

    def __unicode__(self):
        return self.nombres

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'
