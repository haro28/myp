from modeltranslation.translator import register, TranslationOptions

from .models import (Home, InfoSite, SeccionServicios, Servicios, ImagenesServicio,
    NuestraHistoria, QuienesSomos, OtrosPuestos, SeccionPoliticas, Valores,
    Socios ,Logros, Equipamiento, DetalleEquipamiento, SeccionTrabajo, Trabajo,
    ImagenTrabajos, SeccionClientes, Clientes, Contacto)


@register(InfoSite)
class InfoSiteTranslation(TranslationOptions):
    fields = ('direccion',)


@register(Home)
class HomeTranslation(TranslationOptions):
    fields = ('titulo_bienvenida', 'texto_bienvenida')


@register(SeccionServicios)
class SeccionServiciosTranslation(TranslationOptions):
    fields = ('titulo_seccion_servicios', 'texto_servicios')


@register(Servicios)
class ServiciosTranslation(TranslationOptions):
    fields = ('nombre', 'descripcion_servicio', 'slug')


@register(ImagenesServicio)
class ImagenesServicioTranslation(TranslationOptions):
    fields = ('titulo_imagen', )


@register(NuestraHistoria)
class NuestraHistoriaTranslation(TranslationOptions):
    fields = ('titulo', 'texto')


@register(QuienesSomos)
class QuienesSomosTranslation(TranslationOptions):
    fields = ('titulo_quienes_somos', 'texto_quienes_somos', 'titulo_nuestros_socios', 'texto_nuestros_socios')


@register(OtrosPuestos)
class OtrosPuestosTranslation(TranslationOptions):
    fields = ('titulo_puesto',)


@register(SeccionPoliticas)
class SeccionPoliticasTranslation(TranslationOptions):
    fields = ('titulo_mision', 'texto_mision', 'titulo_vision', 'texto_vision',
        'titulo_politicas_ambientales', 'texto_politicas_ambientales', 'pdf')


@register(Valores)
class ValoresTranslation(TranslationOptions):
    fields = ('titulo_valor', 'texto_valor')


@register(Logros)
class LogrosTranslation(TranslationOptions):
    fields = ('logro',)


@register(Socios)
class SociosTranslation(TranslationOptions):
    fields = ('nombre', 'puesto', 'cv')


@register(Equipamiento)
class EquipamientoTranslation(TranslationOptions):
    fields = ('equipamiento',)


@register(DetalleEquipamiento)
class DetalleEquipamientoTranslation(TranslationOptions):
    fields = ('detalle_equipamiento',)


@register(SeccionTrabajo)
class SeccionTrabajoTranslation(TranslationOptions):
    fields = ('titulo_trabajo', 'texto')


@register(Trabajo)
class TrabajoTranslation(TranslationOptions):
    fields = ('titulo_trabajo', 'text_trabajo')


@register(ImagenTrabajos)
class ImagenTrabajoTranslation(TranslationOptions):
    fields = ('titulo_trabajo',)


@register(SeccionClientes)
class SeccionClientesTranslation(TranslationOptions):
    fields = ('titulo_seccion_clientes', 'texto')


@register(Clientes)
class ClientesTranslations(TranslationOptions):
    fields = ('nombre',)



