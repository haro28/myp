# -*- coding: utf-8 -*-

from logging import getLogger

from .models import InfoSite

log = getLogger('django')


def get_info():
    """ Obtiene o crea una instancia de InfoSite en caso de que no exista """

    info, created = InfoSite.objects.get_or_create(pk=1, defaults={
        'email_contacto': 'desarrollo@email.com',
        'telefono': '5555555',
        # 'direccion': 'Av. Sin Nombre #123',
        'facebook': 'http://facebook.com/',
        'site': 'http://127.0.0.1:8000'
    })

    return info
