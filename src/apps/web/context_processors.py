from .util import get_info


def context_infosite(request):
    return {
        'infosite': get_info()
    }
