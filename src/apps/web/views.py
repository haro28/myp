# -*- coding: utf-8 -*-
from logging import getLogger

from django.shortcuts import render, get_object_or_404, redirect
from django.template import RequestContext as ctx
from django.http import JsonResponse

from .models import (Home, SeccionServicios, NuestraHistoria, QuienesSomos, OtrosPuestos, Socios,
    SeccionPoliticas, Valores, Equipamiento, Servicios, SeccionTrabajo, Trabajo, SeccionClientes,
    Clientes)
from .forms import ContactoForm

log = getLogger('django')


def home(request):
    log.info('VIEW: HOME')
    home, created = Home.objects.get_or_create(pk=1)
    servicios = Servicios.objects.all().order_by('position')
    return render(request, 'web/home.html', locals(), context_instance=ctx(request))


def nuestra_historia(request):
    log.info('VIEW: NUESTRA HISTORIA')
    historia, created = NuestraHistoria.objects.get_or_create(pk=1)
    return render(request, 'web/historia.html', locals(), context_instance=ctx(request))


def quienes_somos(request):
    log.info('VIEW: QUIENES SOMOS')
    quienes_somos = QuienesSomos.objects.all()[0]
    otros_puestos_izquiedo = OtrosPuestos.objects.filter(bloque='IZQUIERDO')
    otros_puestos_derecho = OtrosPuestos.objects.filter(bloque='DERECHO')
    socios = Socios.objects.all().order_by('position')

    return render(request, 'web/quienes_somos.html', locals(), context_instance=ctx(request))


def mision_vision_valores(request):
    log.info('VIEW: MISION VISION')
    politica = SeccionPoliticas.objects.all()[0]
    valores = Valores.objects.all().order_by('position')

    return render(request, 'web/politicas.html', locals(), context_instance=ctx(request))


def equipamiento(request):
    log.info('VIEW: EQUIPAMIENTO')
    equipamientos_izquierdo = Equipamiento.objects.filter(bloque='izquierdo').order_by('position')
    equipamientos_derecho = Equipamiento.objects.filter(bloque='derecho').order_by('position')
    equipamientos_central = Equipamiento.objects.filter(bloque='central').order_by('position')

    return render(request, 'web/equipamiento.html', locals(), context_instance=ctx(request))


def ajax_equipamiento_modal(request):
    log.info('AJAX: EQUIPAMIENTO')
    equipamiento_pk = request.GET.get('equipamiento_pk')
    equipamiento = get_object_or_404(Equipamiento, pk=equipamiento_pk)
    detalles = equipamiento.detalleequipamiento_set.all().order_by('position')
    return render(request, 'web/equipamiento_ajax.html', locals(), context_instance=ctx(request))


def servicios(request):
    log.info('VIEWS: SERVICIOS')
    seccion_servicios, created = SeccionServicios.objects.get_or_create(pk=1)
    servicios = Servicios.objects.all().order_by('position')
    return render(request, 'web/servicios.html', locals(), context_instance=ctx(request))


def servicios_detalle(request, slug):
    log.info('VIEW: SERVICIOS DETALLE')
    servicio = get_object_or_404(Servicios, slug=slug)
    imagenes = servicio.imagenesservicio_set.all().order_by('position')

    return render(request, 'web/servicios_detalle.html', locals(), context_instance=ctx(request))


def trabajos(request):
    log.info('VIEW: TRABAJOS')
    seccion_trabajo, created = SeccionTrabajo.objects.get_or_create(pk=1)
    trabajos = Trabajo.objects.all().order_by('position')
    return render(request, 'web/trabajos.html', locals(), context_instance=ctx(request))


def clientes(request):
    log.info('VIEW: CLIENTES')
    seccion_cliente = SeccionClientes.objects.get_or_create(pk=1)
    clientes = Clientes.objects.all().order_by('position')

    return render(request, 'web/clientes.html', locals(), context_instance=ctx(request))


def contacto(request):
    log.info('contacto')
    return render(request, 'web/contact.html', locals(), context_instance=ctx(request))

# AJAX
def contacto_ajax(request):
    log.info('AJAX: CONTACTO')

    if request.is_ajax() and request.method == 'POST':
        form = ContactoForm(request.POST)
        if form.is_valid():
            form.save()
            form.enviaEmail()
            msg = "GRACIAS"
        else:
            log.warning('ERROR DE FORMULARIO')
            msg = "ERROR"

    else:
        msg = 'NO SOY AJAX'
    json = {'msg': msg}

    return JsonResponse(json, content_type='json')
