# -*- coding: utf-8 -*-
from logging import getLogger

from django.forms import ModelForm
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMessage
from django.conf import settings


from .models import (Contacto,)
from .util import get_info


log = getLogger('django')


class ContactoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContactoForm, self).__init__(*args, **kwargs)

        # custom widget attribute:
        # self.fields['nombres'].widget.attrs["Nombres."] = 'nombre'
        # self.fields['email'].widget.attrs["Email."] = 'email'
        # self.fields['telefono'].widget.attrs["Teléfonos."] = 'telefono'
        # self.fields['mensaje'].widget.attrs["Mensaje."] = 'mensaje'

    class Meta:
        model = Contacto
        fields = ('nombres', 'email', 'telefono', 'asunto', 'mensaje', 'compania')

    def enviaEmail(self):
        htmly = get_template('web/email-contacto.html')
        info = get_info()
        c_d = self.cleaned_data
        c_d['info'] = info
        d = Context(c_d)

        html_content = htmly.render(d)
        asunto = u'MYP: Contacto'
        mail = 'MYP<{0}>'.format(settings.DEFAULT_FROM_EMAIL)
        emails_destino = info.email_contacto.split(',')
        print 'EMAILS DESTINO', emails_destino
        msg = EmailMessage(asunto, html_content, mail, emails_destino)
        msg.content_subtype = "html"
        msg.send()
