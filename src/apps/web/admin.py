# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
# Third party app imports
from singlemodeladmin import SingleModelAdmin
from filebrowser.settings import ADMIN_THUMBNAIL
# from translation import *
from modeltranslation.admin import (TabbedExternalJqueryTranslationAdmin as TabTranslationAdmin,
    TranslationTabularInline, TranslationAdmin, TranslationStackedInline)

from .models import (InfoSite, Home, SeccionServicios, Servicios, ImagenesServicio,
    NuestraHistoria, QuienesSomos, OtrosPuestos, SeccionPoliticas, Valores, Socios,
    Logros, Equipamiento, DetalleEquipamiento, SeccionTrabajo, Trabajo, ImagenTrabajos,
    SeccionClientes, Clientes, Contacto)


@admin.register(InfoSite)
class InfoSiteAdmin(TranslationAdmin, SingleModelAdmin):
    fieldsets = (
        ('Información el sitio web', {'fields': ('site', 'ga', 'logo_empresa', 'video', 'activar_idiomas',
            'activar_trabajo', 'activar_clientes')}),
        ('Información de la empresa', {'fields': ('telefono',
            'email', 'direccion', 'coordenadas', 'email_contacto')}),
        ('Enlaces externos', {'fields': ('facebook', 'linkedin', 'gmail', 'twitter', 'youtube')}),
        # ('Mapa', {'fields': ('mapa', )}),
    )


@admin.register(Home)
class HomeAdmin(TranslationAdmin, SingleModelAdmin):
    pass


@admin.register(SeccionServicios)
class SeccionServicios(TranslationAdmin, SingleModelAdmin):
    pass


class ImagenesServiciosInline(TranslationStackedInline):
    model = ImagenesServicio
    ordering = ['position']
    extra = 0


@admin.register(Servicios)
class ServiciosAdmin(TranslationAdmin):
    inlines = [ImagenesServiciosInline]
    list_display = ['nombre', 'position']
    readonly_fields = ['slug']
    list_editable = ['position']


@admin.register(NuestraHistoria)
class NuestraHistoriaAdmin(TranslationAdmin, SingleModelAdmin):
    pass


class OtrosPuestoInline(TranslationStackedInline):
    model = OtrosPuestos
    ordering = ['bloque', 'position']
    extra = 0


@admin.register(QuienesSomos)
class QuienesSomosAdmin(TranslationAdmin, SingleModelAdmin):
    inlines = [OtrosPuestoInline]


@admin.register(SeccionPoliticas)
class SeccionPoliticasAdmin(TranslationAdmin, SingleModelAdmin):
    pass


@admin.register(Valores)
class ValoresAdmin(TranslationAdmin):
    list_display = ['titulo_valor', 'position']
    list_editable = ['position']
    ordering = ['position']


class LogrosInline(TranslationStackedInline):
    model = Logros
    extra = 0
    ordering = ['position']


@admin.register(Socios)
class SociosAdmin(TranslationAdmin):
    inlines = [LogrosInline]
    list_display = ['nombre', 'puesto', 'position']
    list_editable = ['position']
    ordering = ['position']


class DetalleEquipamientoInline(TranslationStackedInline):
    model = DetalleEquipamiento
    ordering = ['position']
    extra = 0


@admin.register(Equipamiento)
class EquipamientoAdmin(TranslationAdmin):
    inlines = [DetalleEquipamientoInline]
    list_display = ['equipamiento', 'bloque', 'position']
    ordering = ['bloque', 'position']
    list_editable = ['bloque', 'position']


@admin.register(SeccionTrabajo)
class SeccionTrabajoAdmin(TranslationAdmin, SingleModelAdmin):
    pass


class ImagenTrabajosInline(TranslationStackedInline):
    model = ImagenTrabajos
    extra = 0
    ordering = ['position']


@admin.register(Trabajo)
class TrabajoAdmin(TranslationAdmin):
    inlines = [ImagenTrabajosInline]
    list_display = ['titulo_trabajo', 'position']
    list_editable = ['position']
    ordering = ['position']


class ClientesInline(TranslationStackedInline):
    model = Clientes
    extra = 0
    ordering = ['position']


@admin.register(SeccionClientes)
class SeccionClientesAdmin(TranslationAdmin, SingleModelAdmin):
    inlines = [ClientesInline]


@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
    readonly_fields = ('fecha', 'nombres', 'email', 'telefono', 'compania',
        'asunto', 'mensaje')
    list_display = ['nombres', 'email', 'telefono']



