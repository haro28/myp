from django.conf.urls import url
from .views import (home, nuestra_historia, quienes_somos, mision_vision_valores,
 equipamiento, ajax_equipamiento_modal, servicios, clientes, trabajos, contacto, contacto_ajax,
 servicios_detalle)

app_name = 'web'

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^nuestra-historia/$', nuestra_historia, name='nuestra_historia'),
    url(r'^quienes-somos/$', quienes_somos, name='quienes_somos'),
    url(r'^mision-vision-valores/$', mision_vision_valores, name='mision_vision_valores'),
    url(r'^equipamiento/$', equipamiento, name='equipamiento'),
    url(r'^equipamiento-ajax/$', ajax_equipamiento_modal, name='ajax_equipamiento_modal'),
    url(r'^servicios/$', servicios, name='servicios'),
    url(r'^servicios-detalle/(?P<slug>[-\w]+)/$', servicios_detalle, name='servicios_detalle'),

    url(r'^trabajos/$', trabajos, name='trabajos'),
    url(r'^clientes/$', clientes, name='clientes'),
    url(r'^contacto/$', contacto, name='contacto'),
    url(r'^contacto-ajax/$', contacto_ajax, name='contacto_ajax'),


]
