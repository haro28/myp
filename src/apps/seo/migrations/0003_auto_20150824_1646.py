# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0002_auto_20150801_1638'),
    ]

    operations = [
        migrations.AddField(
            model_name='seo',
            name='meta_robots',
            field=models.CharField(help_text=b'Por ejemplo: "noindex, nofollow"', max_length=60, verbose_name=b'Etiqueta "meta:robots"', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='seo',
            name='url_canonica',
            field=models.URLField(verbose_name=b'URL Can\xc3\xb3nica', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='seo',
            name='url',
            field=models.CharField(help_text=b'Debe ingresar la url relativa. Por ejemplo: "/contacto/"', unique=True, max_length=200, verbose_name=b'URL', db_index=True),
            preserve_default=True,
        ),
    ]
