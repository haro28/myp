
# -*- coding: utf-8 -*-
# production.py
from .base import *

DEBUG = True
PREPEND_WWW = False

# DIRS


MEDIA_ROOT = ENV.get('MEDIA_ROOT', '')
MEDIA_URL = ENV.get('MEDIA_URL', '')

STATIC_ROOT = ENV.get('STATIC_ROOT', '')
STATIC_URL = ENV.get('STATIC_URL', '')
ALLOWED_HOSTS = ENV.get('ALLOWED_HOSTS', '')

